import sys
import gitlab
import os
import sched
import time
import neo
s = sched.scheduler(time.time, time.sleep)

interval = 10  # Interval to check in seconds
projectId = 278964

if (len(sys.argv) > 1):
    projectId = sys.argv[1]

gl = gitlab.Gitlab('https://gitlab.com',
                   private_token=os.environ['GITLAB_TOKEN'])
# gl = gitlab.Gitlab('https://gitlab.com')
gl.auth()

print("Listening every", interval, "seconds to", projectId)

neo.fill((4, 4, 4))


def check_running():
    project = gl.projects.get(projectId, lazy=True)
    pipelines = project.pipelines.list()
    p = pipelines[0]
    last = 'UNK'
    lastId = 'UNK'
    delay = 2

    # Pipeline 0 status
    print("Most recent pipeline: ", p.id, "at", p.created_at)
    if p.status == "running":
        print("RUNNING")
        neo.fill((4, 4, 4))
    elif p.status == "success":
        print("SUCCESS")
    elif p.status == "failed":
        print("FAILED")
    else:
        print("UNK")

    # Get most recent successs/failure
    for p in pipelines:
        if p.status == "success":
            last = "SUCCESS"
            lastId = p.id
            #s.enter(1, 1, neo.color_cycle(0.1, (0, 255, 0)))
            s.enter(1, 1, neo.fill((0, 255, 0)))
            break
        elif p.status == "failed":
            last = "FAILED"
            lastId = p.id
            #s.enter(1, 1, neo.color_cycle(0.1, (255, 0, 0)))
            s.enter(1, 1, neo.fill((255, 0, 0)))
            break

    print('Last success/failure:', p.id, 'was', last)
    s.enter(interval, 1, check_running)

s.enter(interval, 1, check_running)
s.run()
