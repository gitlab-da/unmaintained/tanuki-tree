import sys
import gitlab
import os
import sched
import time
import neo
s = sched.scheduler(time.time, time.sleep)

interval = 60  # Interval to check in seconds
projectId = 278964
num_pixels = 180

if (len(sys.argv) > 1):
    projectId = sys.argv[1]

gl = gitlab.Gitlab('https://gitlab.com',
                   private_token=os.environ['GITLAB_TOKEN'])
# gl = gitlab.Gitlab('https://gitlab.com')
gl.auth()

print("Listening every", interval, "seconds to", projectId)

neo.fill((4, 4, 4))

def fill_color(color):
    for i in range(num_pixels):
        neo.setPixel(i, color);
        neo.setPixel(num_pixels - i - 1, color);
        time.sleep(0.001)
        if i > num_pixels // 2:
            break

def all_on():
    neo.fill((255, 255, 255))

def all_off():
    neo.fill((4, 4, 4))

def green_only():
    fill_color((0, 200, 0));

def red_only():
   fill_color((200, 0, 0))

def blink_all():
    s.enter(.5, 1, all_on)
    s.enter(.8, 1, all_off)
    s.enter(1, 1, all_on)
    s.enter(1.5, 1, all_off)
    s.enter(1.8, 1, green_only)
    s.enter(2, 1, red_only)
    s.enter(2.2, 1, green_only)
    s.enter(2.4, 1, red_only)
    s.enter(2.6, 1, all_on)
    s.enter(3, 1, all_off)
    s.enter(3.5, 1, all_on)
    s.enter(4, 1, all_off)

def rainbow():
    for i in range(num_pixels):
        if (i % 2) == 0:
            neo.setPixel(i, (200, 0, 0));
            neo.setPixel(num_pixels - i - 1, (200, 0, 0));
        else:
            neo.setPixel(i, (0, 200, 0));
            neo.setPixel(num_pixels - i - 1, (0, 200, 0));
        time.sleep(0.01)

def running_lights():
    neo.stepLights((50, 10, 222));

def check_running():
    project = gl.projects.get(projectId, lazy=True)
    pipelines = project.pipelines.list()
    p = pipelines[0]
    last = 'UNK'
    lastId = 'UNK'
    delay = 2

    # Pipeline 0 status
    print("Most recent pipeline: ", p.id, "at", p.created_at)
    rainbow();
    if p.status == "running":
        print("Current status: RUNNING")
        delay = 6
        rainbow();
        s.enter(3, 1, running_lights);
    elif p.status == "success":
        print("Current status: SUCCESS")
    elif p.status == "failed":
        print("Current status: FAILED")
    else:
        print("Unknown status", p.status)

    # Get most recent successs/failure
    for p in pipelines:
        if p.status == "success":
            last = "SUCCESS"
            lastId = p.id
            s.enter(delay, 1, green_only);
            break
        elif p.status == "failed":
            last = "FAILED"
            lastId = p.id
            s.enter(delay, 1, red_only);
            break

    print('Last success/failure:', p.id, 'was', last)
    s.enter(interval, 1, check_running)

s.enter(interval, 1, check_running)
running_lights();
s.enter(10, 1, rainbow)
s.run()
