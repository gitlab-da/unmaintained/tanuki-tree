import board
import neopixel
import sys
import gitlab
import os
import sched
import time
s = sched.scheduler(time.time, time.sleep)

interval = 20  # Interval to check in seconds
projectId = 22595646
pixel_pin = board.D18
num_pixels = 180
ORDER = neopixel.RGB

pixels = neopixel.NeoPixel(pixel_pin, num_pixels)
#pixels.fill((0, 255, 0))

def strand():
    return pixels

def setPixel(p, color):
    pixels[p] = color;

def fill(color):
    pixels.fill(color)

# prange = range(len(pixels))

# for i in prange:
#     #print(i)
#     pixels[i] = (255, 0, 0)
#     #print(len(pixels) -i - 1)
#     #pixels[len(pixels) - i - 1] = (255, 0, 0)
#     #time.sleep(.2)

#for i in prange:
#    pixels[prange-i] = (255, 0, 0)
def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)


def rainbow_cycle(wait):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = wheel(pixel_index & 255)
        #pixels.show()
        #time.sleep(wait)
    return;

def color_cycle(wait, color):
    for j in range(255):
        for i in range(num_pixels):
            pixel_index = (i * 256 // num_pixels) + j
            pixels[i] = color
        pixels.show()
        time.sleep(wait)
    return;

def stepLights(color):
    sleepTime = .5
    for i in range(0, 25):
       pixels[i] = color;
 
    time.sleep(sleepTime)
    
    for i in range(26, 50):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(51, 75):
       pixels[i] = color;
    
    time.sleep(sleepTime)
    
    for i in range(76, 95):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(96, 115):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(116, 130):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(131, 142):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(143, 154):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(155, 162):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(163, 170):
       pixels[i] = color;
    
    time.sleep(sleepTime)
    
    for i in range(171, 174):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(175, 177):
       pixels[i] = color;

    time.sleep(sleepTime)
    
    for i in range(178, 180):
       pixels[i] = color;



#pixels.fill((255, 255, 255))
#pixels[179] = (255, 255, 0)
#rainbow_cycle(0.1)

# while True:
#     # Comment this line out if you have RGBW/GRBW NeoPixels
#     pixels.fill((255, 0, 0))
#     # Uncomment this line if you have RGBW/GRBW NeoPixels
#     # pixels.fill((255, 0, 0, 0))
#     pixels.show()
#     time.sleep(1)

#     # Comment this line out if you have RGBW/GRBW NeoPixels
#     pixels.fill((0, 255, 0))
#     # Uncomment this line if you have RGBW/GRBW NeoPixels
#     # pixels.fill((0, 255, 0, 0))
#     pixels.show()
#     time.sleep(1)

#     # Comment this line out if you have RGBW/GRBW NeoPixels
#     pixels.fill((0, 0, 255))
#     # Uncomment this line if you have RGBW/GRBW NeoPixels
#     # pixels.fill((0, 0, 255, 0))
#     pixels.show()
#     time.sleep(1)

#     rainbow_cycle(0.001)  # rainbow cycle with 1ms delay per step

