#!/usr/bin/python

import RPi.GPIO as GPIO
import sys
import gitlab
import os
import sched
import time
s = sched.scheduler(time.time, time.sleep)

interval = 20  # Interval to check in seconds
projectId = 22595646

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)

if (len(sys.argv) > 1):
    projectId = sys.argv[1]

# private token or personal token authentication
gl = gitlab.Gitlab('https://gitlab.com',
                   private_token=os.environ['GITLAB_TOKEN'])

# make an API request to create the gl.user object. This is mandatory if you
# use the username/password authentication.
gl.auth()

# group = gl.groups.get(10087220)
# for project in group.projects.list():
#     print(project.name)

def all_on():
    GPIO.output(18, True)
    GPIO.output(23, True)

def all_off():
    GPIO.output(18, False)
    GPIO.output(23, False)

def green_only():
    #print("Green only");
    GPIO.output(18, False)
    GPIO.output(23, True)

def red_only():
   #print("Red only");
   GPIO.output(23, False)
   GPIO.output(18, True)

def blink_all():
    s.enter(.5, 1, all_on)
    s.enter(.8, 1, all_off)
    s.enter(1, 1, all_on)
    s.enter(1.5, 1, all_off)
    s.enter(1.8, 1, green_only)
    s.enter(2, 1, red_only)
    s.enter(2.2, 1, green_only)
    s.enter(2.4, 1, red_only)
    s.enter(2.6, 1, all_on)
    s.enter(3, 1, all_off)
    s.enter(3.5, 1, all_on)
    s.enter(4, 1, all_off)

def check_running():
    project = gl.projects.get(projectId, lazy=True)
    pipelines = project.pipelines.list()
    p = pipelines[0]
    last = 'UNK'
    lastId = 'UNK'
    delay = 2

    # Pipeline 0 status
    print("Most recent pipeline: ", p.id, "at", p.created_at)
    all_on();
    if p.status == "running":
        print("RUNNING")
        delay = 11
        blink_all();
        s.enter(5, 1, blink_all)
    elif p.status == "success":
        print("SUCCESS")
    elif p.status == "failed":
        print("FAILED")
    else:
        print("UNK")

    # Get most recent successs/failure
    for p in pipelines:
        if p.status == "success":
            last = "SUCCESS"
            lastId = p.id
            s.enter(delay, 1, green_only);
            break
        elif p.status == "failed":
            last = "FAILED"
            lastId = p.id
            s.enter(delay, 1, red_only);
            break

    print('Last success/failure:', p.id, 'was', last)
    s.enter(interval, 1, check_running)

s.enter(interval, 1, check_running)
s.enter(.1, 1, all_off)
s.enter(.5, 1, red_only)
s.enter(1, 1, green_only)
s.enter(2, 1, all_off)
s.enter(3, 1, all_on)
s.enter(4, 1, all_off)
s.run()
