# Tanuki Tree

## Project Overview

```mermaid
graph TD;
  a[Project ID];
  b[Every 5 seconds, check pipeline API];
  a-->b;
  c[Is pipeline running?];
  d[Flash Lights];
  b-->c;
  s[Last status?];
  e[Turn tree green];
  f[Turn tree red];
  c -- Yes --> d;
  c -- No --> s;
  s -- Passed --> e;
  s -- Failing --> f;
```

## Usage

Given a project ID, get the most recent pipeline's status as well as the most recent completed pipeline's status.

For the `www-gitlab-com` project, run:

```python
  python start.py 7764
```

Result will look like:

```
Most recent pipeline:  229763066 at 2020-12-14T21:46:59.868Z
RUNNING
Last success/failure: 229761729 was SUCCESS
```

Where pipeline 229763066 is still running. Most recently completed pipeline 229761729 was a success.
